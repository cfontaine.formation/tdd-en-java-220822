package fr.dawan.fitness;

public class CalculTest {
    
    private int valeur1;
    private int valeur2;
    
    public int getValeur1() {
        return valeur1;
    }
    public void setValeur1(int valeur1) {
        this.valeur1 = valeur1;
    }
    public int getValeur2() {
        return valeur2;
    }
    public void setValeur2(int valeur2) {
        this.valeur2 = valeur2;
    }

    public int addition() {
        return valeur1+valeur2;
    }
    
    public int soustraction() {
        return valeur1-valeur2;
    }
}
