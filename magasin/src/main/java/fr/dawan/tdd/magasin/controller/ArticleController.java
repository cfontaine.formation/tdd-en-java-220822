package fr.dawan.tdd.magasin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.tdd.magasin.dto.ArticleDto;
import fr.dawan.tdd.magasin.service.ArticleService;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {
    
    @Autowired
    ArticleService service;

    @GetMapping(value="/{id}", produces  = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto getArticleById(@PathVariable long id) {
        return service.getById(id);
    }
    
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> getAllArticle(){
        return service.getAll();
    }
    
}
