package fr.dawan.tdd.magasin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.tdd.magasin.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    List<Article> findByNom(String nom);

}
