package fr.dawan.tdd.magasin.service.implement;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.tdd.magasin.dto.ArticleDto;
import fr.dawan.tdd.magasin.repository.ArticleRepository;
import fr.dawan.tdd.magasin.service.ArticleService;
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    ArticleRepository repo;

    @Autowired
    ModelMapper mapper;

    @Override
    public ArticleDto getById(long id) {
        return mapper.map(repo.findById(id).get(), ArticleDto.class);
    }

    @Override
    public List<ArticleDto> getAll() {
       return repo.findAll().stream().map(m->mapper.map(m,ArticleDto.class)).collect(Collectors.toList());
    }
}
