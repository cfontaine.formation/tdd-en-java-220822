package fr.dawan.tdd.magasin.service;

import java.util.List;

import fr.dawan.tdd.magasin.dto.ArticleDto;

public interface ArticleService {

    ArticleDto getById(long id);
    
    List<ArticleDto> getAll();

}
