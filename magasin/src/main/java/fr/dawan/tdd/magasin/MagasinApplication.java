package fr.dawan.tdd.magasin;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MagasinApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagasinApplication.class, args);
	}

	@Bean
	ModelMapper moddelMapper() {
	    return new ModelMapper();
	}
	
}
