package fr.dawan.tdd.magasin;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import fr.dawan.tdd.magasin.controller.ArticleController;
import fr.dawan.tdd.magasin.dto.ArticleDto;

@SpringBootTest
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters = false)
class MagasinApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ArticleController controller;
	
    @Test
    void contextLoads() {
        assertNotNull(controller);
    }
    
    @Test
    public void getArticleById() throws Exception {
        String rep = mockMvc.perform(get("/api/articles/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{id:1,version:0,nom:\"Stylo\",prix:1.0,description:\"un stylo bille\"}", rep, true);
    }
    
//    @Test
//    public void getAllArticle() throws Exception {
//        String rep = mockMvc.perform(get("/api/articles")
//                .accept(MediaType.APPLICATION_JSON)
//                .contentType(MediaType.APPLICATION_JSON_VALUE))
//                .andReturn().getResponse().getContentAsString();
//        JSONAssert.assertEquals("[{id:1,version:0,nom:\"Stylo\",prix:1.0,description:\"un stylo bille\"},{id:2,version:0,nom:\"Souris\",prix:40.0},{id:3,version:0,nom:\"Ecran 4K\",prix:1000.0,0}]", rep, true);
//    }   

}
