package fr.dawan.tdd.magasin.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import fr.dawan.tdd.magasin.dto.ArticleDto;
import fr.dawan.tdd.magasin.entities.Article;
import fr.dawan.tdd.magasin.repository.ArticleRepository;
import fr.dawan.tdd.magasin.service.implement.ArticleServiceImpl;

@ExtendWith(MockitoExtension.class)
class ArticleServiceTest {
    
    @Mock
    private ArticleRepository repo;
    
    @Mock
    private ModelMapper mapper;
    
    @InjectMocks
    private ArticleServiceImpl service;

//    @Test
//    public void articleRepositoryTest() {
//        assertNotNull(repo);
//    }
//    
//    @Test
//    public void articleServiceTest() {
//        assertNotNull(service);
//    }
    
    @Test
    public void getArticleByIdTest() {
        Article a=new Article("nomTest",100.0,"descriptionTest");
        a.setId(1L);
        ArticleDto aDto=new ArticleDto("nomTest",100.0,"descriptionTest");
        aDto.setId(1L);
        when(repo.findById(1L)).thenReturn(Optional.of(a));
        when(mapper.map(a, ArticleDto.class)).thenReturn(aDto);
        ArticleDto repDto=service.getById(1L);
        assertEquals(aDto.getId(),repDto.getId());
        assertEquals(aDto.getNom(),repDto.getNom());
        assertEquals(aDto.getPrix(),repDto.getPrix());
        assertEquals(aDto.getDescription(),repDto.getDescription());
    }
    
    @Test
    public void getAllArticleTest() {
        List<Article> lstA=new ArrayList<>();
        List<ArticleDto> lstDto=new ArrayList<>();
        Article a=new Article("nomTest",100.0,"descriptionTest");
        a.setId(1L);
        lstA.add(a);
        ArticleDto aDto=new ArticleDto("nomTest",100.0,"descriptionTest");
        aDto.setId(1L);
        lstDto.add(aDto);
//        a=new Article("nomTest2",100.0,"descriptionTest2");
//        a.setId(2L);
//        lstA.add(a);
//        aDto=new ArticleDto("nomTest2",100.0,"descriptionTest2");
//        aDto.setId(2L);
//        lstDto.add(aDto);
        //
        when(repo.findAll()).thenReturn(lstA);
        when(mapper.map(a, ArticleDto.class)).thenReturn(aDto);
        List<ArticleDto> lstRep=service.getAll();
        assertNotNull(lstRep);
        ArticleDto repDto=lstRep.get(0);
        assertEquals(1,lstRep.size());
        assertEquals(aDto.getId(),repDto.getId());
        assertEquals(aDto.getNom(),repDto.getNom());
        assertEquals(aDto.getPrix(),repDto.getPrix());
        assertEquals(aDto.getDescription(),repDto.getDescription());
    }

}
