package fr.dawan.tdd.magasin.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.tdd.magasin.entities.Article;

@DataJpaTest
@ActiveProfiles("TEST")
public class ArticleRepositoryTest {
    
    @Autowired
    ArticleRepository repository;
    
    @Test
    public void repositoryTest() {
        assertNotNull(repository);
    }
    
    @Test
    public void findByNomTest(){
       List<Article> lst= repository.findByNom("Stylo");
       assertEquals(1,lst.size());
       assertEquals(1,lst.get(0).getId());
       assertEquals("Stylo",lst.get(0).getNom());
       assertEquals(1.0,lst.get(0).getPrix());
       assertEquals("un stylo bille",lst.get(0).getDescription());
    }
        


}
