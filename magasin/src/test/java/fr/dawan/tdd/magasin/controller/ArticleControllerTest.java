package fr.dawan.tdd.magasin.controller;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import fr.dawan.tdd.magasin.dto.ArticleDto;
import fr.dawan.tdd.magasin.service.ArticleService;

@WebMvcTest(ArticleController.class)
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters = false)
public class ArticleControllerTest {

    @MockBean
    private ArticleService service;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getArticleByIdTest() throws Exception {
        ArticleDto aDto = new ArticleDto("nomTest", 100.0, "descriptionTest");
        aDto.setId(1L);
        when(service.getById(1L)).thenReturn(aDto);
        String rep = mockMvc.perform(get("/api/articles/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
         try {
            JSONAssert.assertEquals("{id:1,version:0,nom:\"nomTest\",prix:100.0,description:\"descriptionTest\"}",
             rep, JSONCompareMode.STRICT);
        } catch (JSONException e) {
            fail();
        }
    }
    
    @Test
    public void getAllArticleTest() throws Exception {
        ArticleDto aDto = new ArticleDto("nomTest", 100.0, "descriptionTest");
        aDto.setId(1L);
        List<ArticleDto> lstDto=new ArrayList<>();
        lstDto.add(aDto);
        when(service.getAll()).thenReturn(lstDto);
        String rep = mockMvc.perform(get("/api/articles")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("[{id:1,version:0,nom:\"nomTest\",prix:100.0,description:\"descriptionTest\"}]", rep, false);
        } catch (JSONException e) {
             fail();
        }
    }
}
