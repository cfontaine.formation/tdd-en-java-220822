package fr.dawan.tdd;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

public class ExempleTest {

    private int valeur = 1;

    @Test
    public void valeurTest() {
        assertEquals(1, valeur);
    }

    @Nested
    @TestInstance(Lifecycle.PER_CLASS)
    class TestImbrique {

        @BeforeAll
        public void setup() {
            System.out.println("Methode setup de la classe imbriquée");
        }

        @BeforeEach
        public void init() {
            valeur = 3;
        }

        @Test
        public void valeurTestImbrique() {
            assertNotEquals(2, valeur);
        }

        @Test
        public void valeurTestImbrique2() {
            assertEquals(3, valeur);
        }

    }

    // Test répté
    @DisplayName("Exemple de test repété")
    @RepeatedTest(value = 4, name = "{displayName} ({currentRepetition}/{totalRepetitions})")
    public void testRepete() {
        assertEquals(1, valeur);
    }

    @DisplayName("Exemple de test repété 2")
    @RepeatedTest(value = 3, name = RepeatedTest.LONG_DISPLAY_NAME)
    public void testRepete2(RepetitionInfo info) {
        System.out.println(info.toString());
        System.out.println(info.getCurrentRepetition());
        System.out.println(info.getTotalRepetitions());
        assertEquals(1, valeur);
    }

    // Test paramétré
    @ParameterizedTest
    @ValueSource(strings = { "Hello", "Bonjour", "tmp" })
    public void parameterizedTest(String param) {
        System.out.println(param);
        assertTrue(param.length() >= 3);
    }

    @ParameterizedTest
    @EnumSource(value = Month.class, mode = Mode.EXCLUDE, names = "MAY")
    public void parameterizedTest(Month mois) {
        assertNotNull(mois);
        System.out.println(mois);
    }

    @ParameterizedTest
    @CsvSource(value = { "2,4", "4,5", "7,10" })
    public void additionTest(int v1, int v2) {
        Calcul c = new Calcul();
        assertEquals(v1 + v2, c.addition(v1, v2));
    }

    @DisplayName("Test parametre source fichier Csv")
    @ParameterizedTest(name="{index} - {displayName}")
    @CsvFileSource(resources = { "/add_data.csv" },delimiter = ';')
    public void additionTestFile(int v1, int v2) {
        Calcul c = new Calcul();
        assertEquals(v1 + v2, c.addition(v1, v2));
    }
    
    @ParameterizedTest
    @MethodSource("provideEvenNumber")
    public void methodParamTest(Integer i) {
        assertTrue(i%2==0);
    }
    
   static  Stream<Integer> provideEvenNumber(){
        return Stream.of(2,4,6,8);
    }
   
   @TestFactory
   Collection<DynamicTest> dynamicTest(){
       Calcul c=new Calcul(); 
       Collection<DynamicTest> res=new ArrayList<>();
       for(int i=0; i<4;i++) {
           int j=i;
          res.add(DynamicTest.dynamicTest("addition:"+i
                   ,()->assertEquals(j+j,c.addition(j,j))));
       }
       return res; 
    }
   
   @DisplayName("exemple d'injection de paramètre")
   @Test
   @Tag("info")
   @Tag("test")
   public void exempleTestInfo(TestInfo info) {
       System.out.println(info.getDisplayName());
       System.out.println(info.getTestClass().get());
       System.out.println(info.getTestMethod().get());
       info.getTags().forEach(System.out::println);
   }
    
}
