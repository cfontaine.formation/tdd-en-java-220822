package fr.dawan.tdd;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class CalculTesTjunit4 {

    private Calcul calcul;
    
    @Before
    public void setUp() throws Exception {
        calcul=new Calcul();
    }

    @Test
    public void additionTest() {
       System.out.println(this);
       assertNotNull(calcul);
       assertEquals(5, calcul.addition(2, 3));
       assertNotEquals(7, calcul.addition(1, 1));
   }
}
