package fr.dawan.tdd;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {
    
    private FizzBuzz fizzBuzz;
    
    @BeforeEach
    public void init() {
        fizzBuzz=new FizzBuzz();
    }
    
    @Test
    public void numberUnModified() {
        assertEquals("2",fizzBuzz.convert(2));
    }
    
    @Test
    public void numberMult3() {
        assertEquals("Fizz",fizzBuzz.convert(3));
        assertEquals("Fizz",fizzBuzz.convert(9));
    }
    
    @Test
    public void numberMult5() {
        assertEquals("Buzz",fizzBuzz.convert(5));
        assertEquals("Buzz",fizzBuzz.convert(10));
    }
    
    @Test
    public void numberMult3AndMult5() {
        assertEquals("FizzBuzz",fizzBuzz.convert(15));
    }

}
