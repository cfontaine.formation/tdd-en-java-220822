package fr.dawan.tdd.suite;

import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;


@SelectPackages({"fr.dawan.tdd"})
@IncludeTags("Calcul")
@Suite
public class SuiteTestTag {

}
