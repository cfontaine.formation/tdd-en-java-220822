package fr.dawan.tdd.suite;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectPackages({"fr.dawan.tdd"})
public class SuiteTestPackage {

}
