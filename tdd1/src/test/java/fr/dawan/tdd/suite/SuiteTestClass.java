package fr.dawan.tdd.suite;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

import fr.dawan.tdd.CalculTest;
import fr.dawan.tdd.OtherTest;
@SelectClasses( {CalculTest.class,OtherTest.class})
@Suite
public class SuiteTestClass {

}
