package fr.dawan.tdd;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
@ExtendWith(MockitoExtension.class)
public class MockitoTest {
    
    @Mock
    private List<String> lstMock;
    
    @InjectMocks
    MgmtString m;
   // Création du mock avec la méthde mock 
//    @BeforeClass
//    public void setupClass() {
//        lstMock=mock(List.class);
//    }

    @Test
    public void testListSize() {
        when(lstMock.size()).thenReturn(3);
        when(lstMock.get(3)).thenReturn("azerty") ;
        System.out.println(lstMock.size());
        System.out.println(lstMock.get(0));
        System.out.println(lstMock.get(3));
        assertEquals(3,lstMock.size());
        assertEquals("azerty",lstMock.get(3));
    }
    
    @Test
    public void testMgmtStringGet() {
        when(lstMock.get(3)).thenReturn("azerty") ;
        assertEquals("azerty",m.get(3));
    }
    
    @Test
    public void testMgmtStringIndexOutOfBound() {
        when(lstMock.get(10)).thenThrow(IndexOutOfBoundsException.class);
        assertThrows(IndexOutOfBoundsException.class, ()->{m.get(10);});
    }
    
    // Méthode static
    @Test
    public void testMethodeStatic() {
        try(MockedStatic<MgmtString> mgmtString=Mockito.mockStatic(MgmtString.class)){
            mgmtString.when(()->MgmtString.size()).thenReturn(10);
            assertEquals(10, MgmtString.size());
        }
    }
    
    //Méthode qui a un retour void
    @Test
    public void testVoidMethode() {
        doThrow(NullPointerException.class).when(lstMock).add(1,null);
        assertThrows(NullPointerException.class, ()->{lstMock.add(1,null);} );
    }
    
    // ArgumentMatcher
    @Test
    public void testArgumentMatcher() {
        when(lstMock.get(anyInt())).thenReturn("hello");
        assertEquals("hello",m.get(4));
        assertEquals("hello",m.get(10));
        assertEquals("hello",m.get(100));
    }
    
    @Test
    public void testArgumentMatcherAny() {
        when(lstMock.contains(any())).thenReturn(true);
        assertTrue(lstMock.contains(null));
        assertTrue(lstMock.contains(123));
    }
    
    @Test
    public void testVerify() {
        List<Integer> lstInt=mock(List.class);
        lstInt.get(3);
        lstInt.get(3);
        verify(lstInt,never()).size();
        verify(lstInt,times(2)).get(3);
    }
    
    @Test
    public void testSpy() {
        List<Integer> lst=new ArrayList<>();
        List<Integer> lstSpy=spy(lst);
        
        lstSpy.add(2);
        lstSpy.add(3);
        lstSpy.add(4);
        assertEquals(3,lstSpy.size());
        
        doReturn(10).when(lstSpy).size();
        assertEquals(10,lstSpy.size());
    }
}


