package fr.dawan.tdd;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumingThat;

import java.awt.Dimension;
import java.io.File;
import java.time.Duration;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.ArraySizeComparator;

@DisplayName("Test de la classe Calcul")
//@Disabled
//@TestInstance(Lifecycle.PER_CLASS)
public class CalculTest {

    private Calcul calcul;

    @BeforeAll
    public static void setUp() {
        System.out.println("SetUp");

    }

    @AfterAll
    public static void tearDown() {
        System.out.println("TearDown");
    }

    @BeforeEach
    public void init() {
        System.out.println("Avant Chaque Test");
        calcul = new Calcul();
    }

    @AfterEach
    public void clear() {
        System.out.println("Après Chaque Test");
    }

    @Test
    @Tag("Calcul")
    @DisplayName("Test de la méthode addition")
    public void additionTest() {
         System.out.println(this);
        assertNotNull(calcul);
        assertEquals(5, calcul.addition(2, 3));
        assertNotEquals(7, calcul.addition(1, 1));
    }

    @Test
    @Tag("Calcul")
    public void isPositifTest() {
        System.out.println(this);
        assertTrue(calcul.isPositif(4));
        assertThat(calcul.isPositif(4)).isTrue();
        assertFalse(calcul.isPositif(-1));
    }

    @Test
    @Tag("Calcul")
    public void divisionByZeroTest() {
        assertThrows(ArithmeticException.class, () -> {
            calcul.divisionEntiere(1, 0);
        });
    }

    @Test
    @Tag("Calcul")
    public void divisionTest() {
        assertEquals(6, calcul.divisionEntiere(12, 2));
    }

    @Test
    @Disabled
    public void inutileTest() {
        System.out.println(this);
    }

    @Test
    @Tag("Exemple")
    public void arrayTest() {
        int t[] = { 2, 4, 7 };
        int exp[] = { 2, 4, 7 };
        assertArrayEquals(exp, t);
        assertThat(t).hasSize(3).containsAnyOf(2, 7);
    }

    @Test
    @Tag("Exemple")
    public void timeOutTest() {
        assertTimeout(Duration.ofMillis(300), () -> {
            calcul.traitement();
        });
    }

    @Test
    @Disabled
    public void verifyDimensionAll() {
        Dimension sut = new Dimension(1000, 500);
        assertAll("Dimmensions non conformes", () -> {
            assertEquals(800, sut.getWidth());
        }, () -> {
            assertEquals(800, sut.getHeight());
        });
    }

    @Test
    @Disabled
    public void verifyDimension() {
        Dimension sut = new Dimension(1000, 500);
        assertEquals(800, sut.getWidth(), "Message personnalisé");
        assertEquals(800, sut.getHeight());
    }

    @Test
    @Tag("Calcul")
    public void instanceCalcul() {
        assertThat(calcul, instanceOf(Calcul.class));
    }

    @Test
    @Tag("Exemple")
    public void statwithTest() {
        String res = "azerty";
        assertThat(res, startsWith("aze"));
    }

    @Test
    @Tag("Exemple")
    public void jsonAssertObjetSimple() throws Exception {
        String exp = "{id:1,nom:\"Doe\"}";
        String jsonRest = "{id:1,nom:\"Doe\",prenom:\"John\"}";

        JSONAssert.assertEquals(exp, jsonRest, JSONCompareMode.LENIENT);

        String expTab = "[{id:2,nom:\"Doe\",prenom:\"Jane\"},{id:1,nom:\"Doe\",prenom:\"John\"}]";
        String jsonRestTab = "[{id:1,nom:\"Doe\",prenom:\"John\"},{id:2,nom:\"Doe\",prenom:\"Jane\"}]";

        JSONAssert.assertEquals(expTab, jsonRestTab, JSONCompareMode.LENIENT);
        String prenoms = "{prenoms:[John, Jane, Alan]}";
        JSONAssert.assertEquals("{prenoms:[3]}", prenoms, new ArraySizeComparator(JSONCompareMode.STRICT));

    }

    @Test
    @Tag("Exemple")
    public void assumeTest() {
        assumeFalse(System.getenv("OS").startsWith("Windows"));
        fail();
    }

    @Test
    @Tag("Exemple")
    void testAssumption() {
        System.out.println(System.getenv("OS"));
        assumingThat(System.getenv("OS").startsWith("Windows"), () -> {
            assertTrue(new File("C:/Windows").exists(), "Repertoire Windows inexistant");
        });
        assertTrue(true);
    }
}
