package fr.dawan.tdd;

public class Calcul {

    public int addition(int a, int b) {
        return a + b;
    }
    
    public int divisionEntiere(int a, int b) {
        return a/b;
    }
    
    public boolean isPositif(int a) {
        return a>=0;
    }
    
    public void traitement() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
