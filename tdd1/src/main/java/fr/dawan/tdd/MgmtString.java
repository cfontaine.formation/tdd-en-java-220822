package fr.dawan.tdd;

import java.util.ArrayList;
import java.util.List;

public class MgmtString {

    private List<String> lst=new ArrayList<>();
    
    public String concat() {
        StringBuffer sb=new StringBuffer();
        for(String s :lst)
        {
            sb.append(s);
        }
        return sb.toString();
    }
    
    public String get(int index) {
        return lst.get(index);
    }
    
    public static int size() {
        return 5;
    }
    //...
    
    public void traitementAge( int age) throws Exception {
       System.out.print("Traitement Age");
        if(age<0) {
            throw new Exception();
        }
    }
}
